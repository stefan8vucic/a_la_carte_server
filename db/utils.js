const dodaj_restoran_postman = {
    "user_name" : "Rest1",
    "password" : "1234",
    "email" : "rest1@restaurant.com",
    "name" : "Rest1", 
    "tell" : "+38765253454", 
    "city" : "City1", 
    "country" : "Country1", 
    "zip" : 99999, 
    "address" : "Address1", 
    "type" : "Grill, Traditional", 
    "description" : "", 
    "open" : "08:00", 
    "close" : "22:00", 
    "logo" : "https://img-corp.com/images/logo@2x.png"
}

const menu = [
    [1000011, "Coffe", "", "Brasilien coffee, the best", 1.5, "drinks", "hot_drinks"], 
    [1000011, "Tea", "", "Black tea, the best", 1.5, "drinks", "hot_drinks"], 
]

const menu2 = [
    [1000011, "Coffee", "", "20ml", "The best Brazilian coffee", 1.5, "drinks", "hot_drinks", "coffee"], 
    [1000011, "Tea", "", "200ml", "The best black tea", 1.5, "drinks", "hot_drinks", "tea"],
    [1000011, "Coca-Cola", "", "250ml", "", 2.5, "drinks", "softs", "carbonated"], 
    [1000011, "Fanta", "", "250ml", "", 2.5, "drinks", "softs", "carbonated"], 
    [1000011, "Orange", "", "200ml", "Freshly squeezed orange juice", 3, "drinks", "softs", "squeezed_juice"], 
    [1000011, "Vegetable soup", "", "portion", "Finest homemade, vegetable mix soup", 3, "food", "soup", ""], 
    [1000011, "Chicken soup", "", "portion", "Finest homemade, chicken and vegetable mix soup", 4, "food", "soup", ""], 
    [1000011, "Beef burger", "", "portion", "150g of finest beef + French fries", 6, "food", "burger", "beef"], 
    [1000011, "Chicken nuggets", "", "portion", "250g of finest chicken + French fries", 6, "food", "nugets", "chicken"] 
]

const tables = [
    [1, 1000031, 4, "free"],
    [2, 1000031, 4, "free"],
    [3, 1000031, 4, "free"],
    [4, 1000031, 4, "free"],
    [5, 1000031, 4, "free"],
]

const tables_query = `INSERT INTO tables (table_no, rest_id, place, status) VALUES ?`

const events = [
    ["https://cdnimg.webstaurantstore.com/uploads/blog/2016/4/acoustic-artist-live-in-bar.jpg", "1000011", "top", "Top Band - Live", "", "music", "", "2021-02-20 20:00:00"], 
    ["https://media-cdn.tripadvisor.com/media/photo-s/08/f2/67/f1/fat-mo-s.jpg", "1000031", "mid", "Extra Band - Live & Paulaner promotion", "Paulaner 0.5l price for the night 3KM", "music", "promotion", "2021-02-20 20:30:00"],
    ["https://www.corkcityfc.ie/home/wp-content/uploads/2018/05/UEFA-Champions-League.jpg", "1000011", "low", "Champions League - Final", "All snacks -20%", "sport", "promotion", "2021-02-21 20:00:00"], 
    ["https://media.timeout.com/images/101619085/630/472/image.jpg", "1000031", "mid", "Table Football Tournament", "Winner gets all drinks free", "sport", "", "2021-02-21 16:00:00"], 
    ["https://image.freepik.com/free-vector/karaoke-neon-signs-style_118419-546.jpg", "1000011", "mid", "Karaoke Tournament", "Winner gets all drinks free", "sport", "music", "2021-02-21 21:00:00"],
    ["https://www.theiwsr.com/wp-content/uploads/Wine-Roundup-700x525px-600x450.jpg", "1000031", "top", '"The Best Wine" promotion', 'All "The Best Wine" products -30% whole week', "promotion", "", "2021-02-22 12:00:00"] 
]

const events_query = `INSERT INTO events (img, rest_id, priority, title, description, type_1, type_2, start) VALUES ?`

const user = {
    "user_name" : "User2",
    "password" : "User2",
    "email" : "user2@mymail.com",
    "phone" : "+387646677889",
    "city" : "Banja Luka",
    "country" : "Bosnia and Herzegovina",
    "zip" : "78000",
    "address" : "User2 address 1",
    "birthday": "2006-2-23",
    "sex" : "f",
    "language" : "eng"
}

const menu3 = [
    [1000011, "Nektar", "", "0.33l", "Mali Nektar najbolji na svijetu", 2.5, "drinks", "Beers", "Nektar"], 
    [1000011, "Nektar", "", "0.50l", "Veliki Nektar najbolji na svijetu", 2.0, "drinks", "Beers", "Nektar"],
    [1000011, "Tuborg", "", "0.50l", "Veliki Tuborg", 2.5, "drinks", "Beers", "Tuborg"], 
    [1000011, "Jelen", "", "0.50l", "Veliki Jelen", 2.0, "drinks", "Beers", "Jelen"],
    [1000011, "Jelen", "", "0.33l", "Mali Jelen", 2.5, "drinks", "Beers", "Jelen"],
    [1000011, "Bavaria", "", "0.25l", "Mala Bavaria", 2.5, "drinks", "Beers", "Bavaria"]
]
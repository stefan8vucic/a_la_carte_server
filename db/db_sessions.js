const { pool } = require("./db_services");

const sessions = {}

sessions.check_r_session = (id) => {
    
    return new Promise ((resolve, reject) => {
        pool.query(`SELECT * FROM sesije WHERE r_id =?`, [id], (err, res) => {
            if(err) return reject (err);
            return resolve(res);
        });
    });
};

const create_r_sesion_q = "INSERT INTO sesije (r_id, r_ref_token, r_socket_id) VALUES ?"

sessions.create_r_session = (data) => {
    console.log(data)
    let values = [[data.r_id, data.r_ref_token, data.r_socket_id]];
    console.log(values)
    return new Promise ((resolve, reject) => {
        pool.query(create_r_sesion_q, [values], (err, res) => {
            if(err) return reject (err);
            return resolve(res);
        });
    });
};

sessions.update_r_session = (data) => {
    return new Promise ((resolve, reject) => {
        pool.query(`UPDATE sesije SET r_socket_id = "${data.r_socket_id}" where r_id =?`, [data.r_id], (err, res) => {
            if(err) return reject (err);
            return resolve(res);
        });
    });
};

sessions.delete_r_session = (id) => {
    console.log(id);
    return new Promise ((resolve, reject) => {
        pool.query(`DELETE FROM sesije WHERE r_socket_id =?`, [id], (err, res) => {
            if(err) return reject (err);
            return resolve(res);
        });
    });
};

module.exports = sessions;
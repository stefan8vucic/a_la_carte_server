const mySql = require("mysql");
const { env } = require("process");
require("dotenv/config");
const sessions = require("./db_sessions");

const pool = mySql.createPool({
    connectionLimit: 100,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    database: process.env.DB_DB,
    port: 3306
});

const query2 = "INSERT INTO restorani (r_name, r_password, r_zip) VALUES ?"

const create_table_q = "CREATE TABLE sesije (id INT AUTO_INCREMENT PRIMARY KEY, r_name VARCHAR(255), r_password VARCHAR(255), r_zip INT, r_registration TIMESTAMP DEFAULT CURRENT_TIMESTAMP ) ENGINE=InnoDB AUTO_INCREMENT=100000"

const create_restaurants_profile_table = "CREATE TABLE restaurants_profile ( id INT PRIMARY KEY, name VARCHAR(255), email VARCHAR(255), tell VARCHAR(255), city VARCHAR(255), country VARCHAR(255), zip INT, address VARCHAR(255), type VARCHAR(255), description VARCHAR(1000), open VARCHAR(255), close VARCHAR(255), logo VARCHAR(1000))"

const create_menu_table = "CREATE TABLE menu (id INT AUTO_INCREMENT PRIMARY KEY, rest_id INT, name VARCHAR(255), img VARCHAR(1000), volume VARCHAR(55),  description VARCHAR(255), price DECIMAL(5,2), type VARCHAR(255), category VARCHAR(255), subcategory VARCHAR(255), creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP ) ENGINE=InnoDB AUTO_INCREMENT=1000000"

const create_orders_table = "CREATE TABLE orders (id INT AUTO_INCREMENT PRIMARY KEY, user_id INT, rest_id INT, table_no INT, status VARCHAR(12), substatus VARCHAR(12), message VARCHAR(255), creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP ) ENGINE=InnoDB AUTO_INCREMENT=1000000"

const create_order_list_table = "CREATE TABLE order_items ( order_id INT, item_id INT, quantity INT, price DECIMAL(5,2))"

const create_visits_table = "CREATE TABLE visits ( visitor_id INT, rest_id INT, visited TIMESTAMP DEFAULT CURRENT_TIMESTAMP)"

const create_tables = "CREATE TABLE tables ( id INT AUTO_INCREMENT PRIMARY KEY, table_no INT, rest_id INT, place INT, status VARCHAR(55)) ENGINE=InnoDB AUTO_INCREMENT=1000000";

const create_tables_eva = "CREATE TABLE evaluation ( id INT AUTO_INCREMENT PRIMARY KEY, user_id INT, rest_id INT, order_id INT, menu INT, service INT, ambience INT, comment VARCHAR(300), creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP) ENGINE=InnoDB AUTO_INCREMENT=1000000";

const create_events = "CREATE TABLE events ( id INT AUTO_INCREMENT PRIMARY KEY, img VARCHAR(255), rest_id INT, priority VARCHAR(10), title VARCHAR(255), description VARCHAR(255), type_1 VARCHAR(55), type_2 VARCHAR(55), start TIMESTAMP) ENGINE=InnoDB AUTO_INCREMENT=1000000";

const create_reservations = "CREATE TABLE reservations ( id INT AUTO_INCREMENT PRIMARY KEY, rest_id INT, user_id INT, persons INT, event_id INT, status VARCHAR(12), substatus VARCHAR(12), start TIMESTAMP, creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP ) ENGINE=InnoDB AUTO_INCREMENT=1000005";

const create_reservations_messages = "CREATE TABLE reservations_msg ( id INT AUTO_INCREMENT PRIMARY KEY, reservation_id INT, sender INT, status VARCHAR(12), message VARCHAR(300), creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP ) ENGINE=InnoDB AUTO_INCREMENT=1000005";

const create_user_login = "CREATE TABLE user_profile ( id INT AUTO_INCREMENT PRIMARY KEY, user_name VARCHAR(20), password VARCHAR(200), email VARCHAR(100), phone VARCHAR(50), birthday DATE, city VARCHAR(50), zip INT, address VARCHAR(255), country VARCHAR(50), sex VARCHAR(5), language VARCHAR(50), last_login TIMESTAMP, creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP ) ENGINE=InnoDB AUTO_INCREMENT=2000000";

const user_query = `INSERT INTO user_profile (user_name, password, email, phone, birthday, city, country, sex, language) VALUES ?`

const create_r_sesion_q = "INSERT INTO sesije (r_id, r_ref_token, r_socket_id) VALUES ?"

const delete_table = "DROP TABLE order_items"

const db = {}

db.delete_table = () => {
    return new Promise((resolve, reject) => {
        pool.query(delete_table, (err, res) => {
            if (err) return reject(err);
            console.log("obrisano")
            return resolve(res);
        });
    });
};

db.create_table = () => {
    return new Promise((resolve, reject) => {
        pool.query(create_order_list_table, (err, res) => {
            if (err) return reject(err);
            console.log("kreirano")
            return resolve(res);
        });
    });
};



const events_query = `INSERT INTO events (img, rest_id, priority, title, description, type_1, type_2, start) VALUES ?`

db.insert_multiple = (data) => {
    return new Promise((resolve, reject) => {
        pool.query(user_query, [data], (err, res) => {
            if (err) return reject(err);
            console.log("Upisano")
            return resolve(res);
        });
    });
};

db.insertInTable = (data) => {
    let values;
    if (table.includes("login")) {
        values = [[data.user_name, data.password, data.email]];

        return new Promise((resolve, reject) => {
            pool.query(`INSERT INTO ${table} (user_name, password, email) VALUES ?`, [values], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    }
    else {
        values = [[data.id, data.name, data.email, data.tell, data.city, data.country, data.zip, data.address, data.type, data.description, data.open, data.close, data.logo]];

        return new Promise((resolve, reject) => {
            pool.query(`INSERT INTO ${table} (id, name, email, tell, city, country, zip, address, type, description, open, close, logo) VALUES ?`, [values], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    };
};

db.delete_item = () => {
    return new Promise((resolve, reject) => {
        pool.query(`DELETE FROM reservations WHERE id = 1000011`, (err, res) => {
            if (err) return reject(err);
            console.log("obrisano")
            return resolve(res);
        });
    });
};

db.get_all = (table, data) => {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM ${table}`, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.get_filtered_rest = (table, data) => {
    return new Promise((resolve, reject) => {
        let query_handler = {
            join: "INNER",
            condition: `WHERE ${table}.city = ? AND ${table}.type LIKE ? AND tables.status =?`,
            data: [data.city, `%${data.type}%`, "free"]
        };
        if (!data.name) {
            if (data.city) {
                if (data.type) {
                    if (!data.open) {
                        query_handler.join = "LEFT"
                    };
                }
                else {
                    query_handler.condition = `WHERE city = ? AND tables.status = ?`;
                    query_handler.data = [data.city, 'free'];
                    if (!data.open) {
                        query_handler.join = "LEFT"
                    };
                };
            }
            else {
                if (data.type) {
                    query_handler.condition = `WHERE type LIKE ? AND tables.status = ?`;
                    query_handler.data = [`%${data.type}%`, 'free'];
                    if (!data.open) {
                        query_handler.join = "LEFT";
                    };
                }
                else {
                    query_handler.condition = `WHERE tables.status = ?`;
                    query_handler.data = ['free'];
                    if (!data.open) {
                        query_handler.join = "LEFT";
                    };
                };
            };
        }
        else {
            query_handler.join = "LEFT";
            query_handler.condition = `WHERE tables.status = ? AND ${table}.name LIKE ?`;
            query_handler.data = ['free', `%${data.name}%`];
        };

        pool.query(
            `SELECT ${table}.*, COUNT(tables.status) AS tables, sesije.r_id AS status
            FROM ((${table} 
            INNER JOIN tables ON ${table}.id = tables.rest_id)
            ${query_handler.join} JOIN sesije ON ${table}.id = sesije.r_id)
            ${query_handler.condition}
            GROUP BY ${table}.id`, query_handler.data, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.get_filter_data = (city) => {
    let query;
    if (city) {
        query = `SELECT city 
        FROM restaurants_profile 
        WHERE city LIKE '${city}%'
        GROUP BY city`
    }
    else {
        query = `SELECT city FROM restaurants_profile GROUP BY city`
    }
    return new Promise((resolve, reject) => {
        pool.query(query, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.get_one = async (id, table) => {
    console.log(id);
    const query_with_tables = `SELECT ${table}.*, COUNT(tables.status) AS tables, sesije.r_id AS status, AVG(evaluation.menu + evaluation.ambience + evaluation.service) / 3 AS eval_avg, COUNT(evaluation.rest_id) AS eval_count
    FROM (((${table}   
    INNER JOIN tables ON ${table}.id = tables.rest_id)
    LEFT JOIN evaluation ON ${table}.id = evaluation.rest_id)
    LEFT JOIN sesije ON ${table}.id = sesije.r_id)
    
    WHERE ${table}.id = ? AND tables.status =?
    GROUP BY ${table}.id`
    try {
        const menu = await db.get_menu(id, "menu", "");
        console.log(menu)
        if(menu.length) {
            return new Promise((resolve, reject) => {
                pool.query(
                    `SELECT ${table}.*, sesije.r_id AS status, AVG(evaluation.menu + evaluation.ambience + evaluation.service) / 3 AS eval_avg, COUNT(evaluation.rest_id) AS eval_count
                    FROM ((${table}   
                    LEFT JOIN evaluation ON ${table}.id = evaluation.rest_id)
                    LEFT JOIN sesije ON ${table}.id = sesije.r_id)
                    WHERE ${table}.id = ?
                    GROUP BY ${table}.id`, [id], (err, res) => {
                    if (err) return reject(err);
                    res[0].menu = menu;
                    return resolve(res);
                });
            });
        }
        else {
            return menu;
        };
    }
    catch (err) {
        console.log(err);
        return (err);
    };
};

db.check_user = (user_name, table) => {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM ${table} WHERE user_name =?`, [user_name], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};


db.registration = (data, table) => {
    let values;
    if (table.includes("login")) {
        values = [[data.user_name, data.password, data.email]];

        return new Promise((resolve, reject) => {
            pool.query(`INSERT INTO ${table} (user_name, password, email) VALUES ?`, [values], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    }
    else {
        values = [[data.id, data.name, data.email, data.tell, data.city, data.country, data.zip, data.address, data.type, data.description, data.open, data.close, data.logo]];

        return new Promise((resolve, reject) => {
            pool.query(`INSERT INTO ${table} (id, name, email, tell, city, country, zip, address, type, description, open, close, logo) VALUES ?`, [values], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    };
};

db.post = (data, table) => {
    let values;

    values = [[data.user_name, data.password, data.email]];

    return new Promise((resolve, reject) => {
        pool.query(`INSERT INTO ${table} (r_name, r_password, r_zip) VALUES ?`, [values], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
}

db.check_r_session = (id) => {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM sesije WHERE r_id =?`, [id], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.create_r_session = (data) => {
    console.log(data)
    let values = [[data.r_id, data.r_ref_token, data.r_socket_id]];
    console.log(values)
    return new Promise((resolve, reject) => {
        pool.query(create_r_sesion_q, [values], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.update_r_session = (data) => {
    return new Promise((resolve, reject) => {
        pool.query(`UPDATE sesije SET r_socket_id = "${data.r_socket_id}" where r_id =?`, [data.r_id], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.delete_r_session = (id) => {
    console.log(id);
    return new Promise((resolve, reject) => {
        pool.query(`DELETE FROM sesije WHERE r_id = ?`, [id], (err, res) => {
            if (err) return reject(err);
            console.log("obrisano")
            return resolve(res);
        });
    });
};

db.update_menu = (data) => {
    return new Promise((resolve, reject) => {
        pool.query(`INSERT INTO menu (rest_id, name, img, volume, description, price, type, category, subcategory) VALUES ?`, [data], (err, res) => {
            if (err) return reject(err);
            console.log("Upisano")
            return resolve(res);
        });
    });
};

db.get_menu = (id, table, query) => {
    if (query.column) {
        console.log("categ")
        return new Promise((resolve, reject) => {
            pool.query(`SELECT * FROM ${table} WHERE rest_id = ${id} AND ${query.column} = '${query.value}'`, (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    }
    else {
        return new Promise((resolve, reject) => {
            pool.query(`SELECT * FROM ${table} WHERE rest_id =?`, [id], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    };
};

// za pretragu artikla kasnije!!
db.get_menu_items = (table) => {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM ${table} WHERE name LIKE "%chic%"`, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.update_order = (data) => {
    const values = [[data.user_id, data.rest_id, data.table_no, data.status, data.substatus, data.message]]
    return new Promise((resolve, reject) => {
        pool.query(`INSERT INTO orders (user_id, rest_id, table_no, status, substatus, message) VALUES ?`, [values], (err, res) => {
            if (err) return reject(err);
            console.log("Upisano")
            return resolve(res);
        });
    });
};

db.update_order_status = (data) => {
    const { id, status, substatus } = data;
    console.log(data)
    return new Promise((resolve, reject) => {
        pool.query(`UPDATE orders SET status = "${status}", substatus = "${substatus}" where id =?`, [id], (err, res) => {
            if (err) return reject(err);
            console.log("Ispravljeno");
            return resolve(res);
        });
    });
};

db.get_active_orders = (id) => {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM orders WHERE (status = "active" OR status = "confirmed" OR status = "Served" OR status = "Bill please") AND rest_id =?`, [id], (err, res) => {
            if (err) return reject(err);

            if (res.length) {
                let order_ids = "";
                for (let i = 0; i < res.length; i++) {
                    if (i !== res.length - 1) {
                        order_ids += `order_items.order_id = ${res[i].id} OR `
                    }
                    else {
                        order_ids += `order_items.order_id = ${res[i].id}`
                    };
                };

                pool.query(`SELECT order_items.order_id, order_items.quantity, order_items.price, menu.name
                        FROM order_items
                        INNER JOIN menu ON order_items.item_id = menu.id
                        WHERE ${order_ids}`, (err, res2) => {
                    if (err) return reject(err);
                    pool.query(`SELECT SUM(price * quantity) AS total, order_id FROM order_items WHERE ${order_ids} GROUP BY order_id `, (err, res3) => {
                        if (err) return reject(err);

                        console.log(res3);

                        res.forEach(order => {
                            const order_items = res2.filter(items => items.order_id === order.id);
                            const order_total = res3.find(total => total.order_id === order.id).total;

                            order.list = order_items;
                            order.total = order_total;
                        });
                        return resolve(res);
                    });
                });
            }
            else {
                return resolve(res);
            };

        });
    });
};

db.get_users_orders2 = (id) => {
    console.log("users_orders", id);
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM orders WHERE user_id =?`, [id], (err, res) => {
            if (err) return reject(err);

            if (res.length) {
                let order_ids = "";
                let rest_ids = "";
                for (let i = 0; i < res.length; i++) {
                    if (i !== res.length - 1) {
                        order_ids += `order_items.order_id = ${res[i].id} OR `
                        rest_ids += `id = ${res[i].rest_id} OR `
                    }
                    else {
                        order_ids += `order_items.order_id = ${res[i].id}`
                        rest_ids += `id = ${res[i].rest_id}`
                    };
                };
                pool.query(`SELECT order_items.order_id, order_items.quantity, order_items.price, menu.name
                        FROM order_items
                        INNER JOIN menu ON order_items.item_id = menu.id
                        WHERE ${order_ids}`, (err, res2) => {
                    if (err) return reject(err);
                    pool.query(`SELECT SUM(price * quantity) AS total, order_id FROM order_items WHERE ${order_ids} GROUP BY order_id `, (err, res3) => {
                        if (err) return reject(err);

                        pool.query(`SELECT name, id FROM restaurants_profile WHERE ${rest_ids}`, (err, res4) => {
                            if (err) return reject(err);


                            console.log(res4)
                            res.forEach(order => {
                                order.list = res2.filter(items => items.order_id === order.id);
                                order.total = res3.find(total => total.order_id === order.id).total;
                                order.rest_name = res4.find(rest => rest.id === order.rest_id).name;
                            });
                            return resolve(res);
                        });
                    });
                });
            }
            else {
                return resolve(res);
            };
        });
    });
};


db.get_users_orders = (data) => {
    console.log("users_orders", data);
    const { id, status } = data;

    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT orders.*, restaurants_profile.name, SUM(order_items.price * order_items.quantity) AS total
            FROM ((orders 
            INNER JOIN restaurants_profile ON orders.rest_id = restaurants_profile.id)
            INNER JOIN order_items ON orders.id = order_items.order_id)
            WHERE orders.user_id =? AND orders.status =?
            GROUP BY orders.id
            LIMIT 5`, [id, status], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};



db.update_order_items = (data) => {
    return new Promise((resolve, reject) => {
        pool.query(`INSERT INTO order_items (order_id, item_id, quantity, price) VALUES ?`, [data], (err, res) => {
            if (err) return reject(err);
            console.log("Upisano");
            return resolve(res);
        });
    });
};

db.update_visit = (data) => {
    const values = [[data.visitor_id, data.rest_id]];
    return new Promise((resolve, reject) => {
        pool.query(`INSERT INTO visits (visitor_id, rest_id) VALUES ?`, [values], (err, res) => {
            if (err) return reject(err);
            console.log("Upisana posjeta")
            return resolve(res);
        });
    });
};

db.get_visits_count = (id) => {
    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT COUNT(visits.rest_id) AS total_visits, restaurants_profile.name AS name
            FROM visits
            INNER JOIN restaurants_profile ON visits.rest_id = restaurants_profile.id
            WHERE visits.rest_id =?`, [id], (err, res) => {
            if (err) return reject(err);
            const date_handler = new Date();
            date_handler.setDate(date_handler.getDate() - 1);
            const date = `${date_handler.getFullYear()}-${date_handler.getMonth() + 1}-${date_handler.getDate()} 23:00:00`;
            pool.query(`SELECT COUNT(rest_id) AS today_visit FROM visits WHERE rest_id = ${id} AND visited >= '${date}'`, (err, res2) => {
                if (err) return reject(err);
                res[0].today_visit = res2[0].today_visit;
                console.log(res2, res);
                return resolve(res);
            });
        });
    });
};

db.get_free_tables = (id) => {
    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT id, table_no
            FROM tables
            WHERE status = ? AND rest_id =?`, ['free', id], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.update_table_status = (data) => {
    return new Promise((resolve, reject) => {
        pool.query(
            `UPDATE tables SET status = "${data.status}" where rest_id =? AND table_no =?`, [data.rest_id, data.table_no], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
    });
};

db.get_events = (data) => {
    return new Promise((resolve, reject) => {
        let query_handler = `WHERE restaurants_profile.city = '${data.city}'`;
        if (data.type) {
            query_handler += ` AND events.type_1 = '${data.type}' OR events.type_2 = '${data.type}'`;
        };

        if (data.id) {
            query_handler = `WHERE events.rest_id = ${data.id}`;
        };

        pool.query(
            `SELECT events.*, restaurants_profile.name AS name, restaurants_profile.city AS city
            FROM events
            INNER JOIN restaurants_profile ON restaurants_profile.id = events.rest_id
            ${query_handler}
            ORDER BY CASE 
            WHEN events.priority = 'top' THEN '1'
            WHEN events.priority = 'mid' THEN '2'
            ELSE events.priority END
            LIMIT ${data.limit}
            `, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.update_evaluation = (data) => {
    const values = [[data.user_id, data.rest_id, data.order_id, data.menu, data.ambience, data.service, data.comment]]
    return new Promise((resolve, reject) => {
        pool.query(
            `INSERT INTO evaluation 
            (user_id, rest_id, order_id, menu, ambience, service, comment ) 
            VALUES ?`, [values], (err, res) => {
            if (err) return reject(err);
            console.log("Upisano")
            return resolve(res);
        });
    });
};

db.get_evaluations = (data) => {
    console.log(data)
    let query = "rest_id"
    // if(data.type = "user") query = "user_id";
    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT AVG(menu) AS menu_avg, AVG(ambience) AS ambience_avg, AVG(service) AS service_avg, AVG(menu + service + ambience) / 3 AS review_avg, COUNT(rest_id) AS count
            FROM evaluation
            WHERE ${query} =?`, [data.id], (err, res) => {
            if (err) return reject(err);
            pool.query(
                `SELECT user_id, comment, creation, (menu + service + ambience) / 3 AS review_avg
                FROM evaluation
                WHERE NOT comment = '' AND ${query} =? 
                ORDER BY creation DESC
                LIMIT ${data.limit}`, [data.id], (err, res2) => {
                if (err) return reject(err);

                res[0].comments = res2;

                return resolve(res);
            });
        });
    });
};

db.update_reservation = (data) => {
    let date_handler = `${data.date} ${data.time}`
    console.log("date", date_handler)
    const reservation_values = [[data.user_id, data.rest_id, data.persons, data.status, data.substatus, date_handler, data.event]]
    return new Promise((resolve, reject) => {
        pool.query(
            `INSERT INTO reservations 
            (user_id, rest_id, persons, status, substatus, start, event_id) 
            VALUES ?`, [reservation_values], async (err, res) => {
            if (err) return reject(err);
            if (data.message) {
                data.reservation_id = res.insertId;
                try {
                    db.update_reservation_msg(data);
                }
                catch (err) {
                    return reject(err);
                };
            };
            return resolve(res);
        });
    });
};

db.update_reservation_msg = (data) => {
    if (data.message) {
        const message_values = [[data.reservation_id, data.sender, data.msg_status, data.message]];
        return new Promise((resolve, reject) => {
            pool.query(
                `INSERT INTO reservations_msg 
                (reservation_id, sender, status, message) 
                VALUES ?`, [message_values], (err, res) => {
                if (err) return reject(err);
                pool.query(
                    `SELECT *
                    FROM reservations_msg
                    WHERE reservation_id = ${data.reservation_id}
                    ORDER BY creation`, (err, res2) => {
                    if (err) return reject(err);

                    console.log(res2)
                    return resolve(res2);
                });
            });
        });
    };
};




db.get_reservations = (data) => {
    console.log("reservation", data)
    let query = "reservations.rest_id";
    let name_query = "user_profile";

    if (data.type === "user") {
        query = "reservations.user_id";
        name_query = "restaurants_profile";
    };

    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT reservations.*, ${name_query}.name AS name, events.title AS event_name
            FROM reservations
            INNER JOIN ${name_query} ON reservations.rest_id = restaurants_profile.id
            LEFT JOIN events ON reservations.event_id = events.id
            WHERE ${query} =? AND status =?
            LIMIT ${data.limit}`, [data.id, data.status], (err, res) => {
            if (err) return reject(err);

            if (res.length) {
                let reservations_ids = "";

                for (let i = 0; i < res.length; i++) {
                    if (i !== res.length - 1) {
                        reservations_ids += `reservation_id = ${res[i].id} OR `
                    }
                    else {
                        reservations_ids += `reservation_id = ${res[i].id}`
                    };
                };
                pool.query(
                    `SELECT *
                    FROM reservations_msg
                    WHERE ${reservations_ids}
                    ORDER BY creation`, (err, res2) => {
                    if (err) return reject(err);

                    res.forEach(reservation => {
                        const order_items = res2.filter(items => items.reservation_id === reservation.id);
                        reservation.messages = order_items;
                    });

                    return resolve(res);
                });
            }
            else {
                return resolve(res);
            };
        });
    });
};


db.user_registration = (data) => {
    console.log(data)
    let values = [[data.user_name, data.password, data.email, data.phone, data.birthday, data.city, data.country, data.zip, data.address, data.sex, data.language]];

    return new Promise((resolve, reject) => {
        pool.query(`INSERT INTO user_profile (user_name, password, email, phone, birthday, city, country, zip, address, sex, language) VALUES ?`, [values], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};


module.exports = db

// module.exports.pool = pool.getConnection;


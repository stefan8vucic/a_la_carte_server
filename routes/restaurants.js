const express = require("express");
const restaurants_route = express.Router();
const db = require("../db/db_services");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const table_profile = "restaurants_profile";
const table_login = "restaurants_login";
const table_session = "sesije";

restaurants_route.get("/", async (req, res) => {
    console.log(req.query)
    try {
        const restaurants = await db.get_filtered_rest(table_profile, req.query);
        res.json(restaurants);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.get("/filterdata", async (req, res) => {
    console.log(req.query)
    try {
        const restaurant = await db.get_filter_data(req.query.city);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.get("/profile/:id", async (req, res) => {
    try {
        const restaurant = await db.get_one(req.params.id, table_profile);
        console.log("restaurant", restaurant)
        if (restaurant.length) {
            res.json(restaurant);
        }
        else {
            return res.status(404).json({ message: "UserName of Password incorrect" });
        };
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.get("/session/:id", async (req, res) => {
    try {
        const restaurant = await db.check_r_session(req.params.id);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.get("/menu/:id", async (req, res) => {

    console.log(req.params.id, req.query)
    try {
        const restaurant = await db.get_menu(req.params.id, "menu", req.query);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});



restaurants_route.post("/menu", async (req, res) => {

    console.log(req.body)
    try {
        const restaurant = await db.update_menu(req.body);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.post("/registration", async (req, res) => {
    try {
        const salt = await bcrypt.genSalt(10);
        const hashed_pass = await bcrypt.hash(req.body.password, salt);
        req.body.password = hashed_pass;
        const login_inserted = await db.registration(req.body, table_login);
        req.body.id = login_inserted.insertId;
        const profile_inserted = await db.registration(req.body, table_profile);
        // const inserted_restaurant = await db.get_one(inserted.insertId, table);
        // res.json(inserted_restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.post("/login", async (req, res) => {
    try {
        console.log(req.body)
        const user = await db.check_user(req.body.user_name, table_login);
        console.log(user);
        if (!user.length) return res.status(401).json({ message: "Not found" });

        const password_check = await bcrypt.compare(req.body.password, user[0].password);
        if (!password_check) return res.status(401).json({ message: "Not found" });

        const rest = await db.get_one(user[0].id, table_profile);

        const access_token = create_jwt(user[0].id, "");
        const ref_token = jwt.sign(user[0].id, process.env.REFRESH_TOCKEN_SECRET);

        res.json({ access_token, ref_token, id: user[0].id, name: rest[0].name })
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.get("/order/:id", async (req, res) => {
    console.log(req.params.id);
    try {
        const restaurant = await db.get_active_orders(req.params.id);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.post("/order", async (req, res) => {
    console.log(req.body)
    try {
        const restaurant = await db.update_order(req.body);
        res.json(restaurant.insertId);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.patch("/order", async (req, res) => {
    console.log(req.body)
    try {
        const restaurant = await db.update_order_status(req.body);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});




restaurants_route.post("/order-items", async (req, res) => {
    console.log(req.body)
    try {
        const restaurant = await db.update_order_items(req.body);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.get("/visit/:id", async (req, res) => {
    console.log(req.params.id);
    try {
        const restaurant = await db.get_visits_count(req.params.id);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.post("/visit", async (req, res) => {
    console.log("visit" , req.body)
    try {
        const restaurant = await db.update_visit(req.body);
        res.json(restaurant.insertId);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.get("/tables/:id", async (req, res) => {
    try {
        const restaurant = await db.get_free_tables(req.params.id);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.patch("/tables", async (req, res) => {
    try {
        const restaurant = await db.update_table_status(req.body);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.get("/events", async (req, res) => {
    console.log(req.query)
    try {
        const restaurant = await db.get_events(req.query);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.post("/evaluation", async (req, res) => {
    console.log("evaluation" , req.body)
    try {
        const restaurant = await db.update_evaluation(req.body);
        res.json(restaurant.insertId);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.get("/evaluation", async (req, res) => {
    try {
        const restaurant = await db.get_evaluations(req.query);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.post("/reservations", async (req, res) => {
    try {
        const restaurant = await db.update_reservation(req.body);
        res.json(restaurant.insertId);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.post("/reservations_msg", async (req, res) => {
    try {
        const restaurant = await db.update_reservation_msg(req.body);
        res.json(restaurant.insertId);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

restaurants_route.get("/reservations", async (req, res) => {
    try {
        const restaurant = await db.get_reservations(req.query);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});



const create_jwt = (user_id, ref_token) => {
    if (!ref_token) {
        return jwt.sign(user_id, process.env.AUTH_TOCKEN_SECRET);
    }
    else {
        return jwt.sign({ id: user_id }, ref_token, {
            expiresIn: '30m'
        });
    };
};




module.exports = restaurants_route;

const express = require("express");
const router = express.Router();
const db = require("../db/db_services");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const verify = require("./token_verification");

const table = "users"

router.get("/", async (req, res) => {
    try {
        const users = await db.get_all(table);
        res.json(users);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

// router.get("/:id", async (req, res) => {
//     try {
//         const user = await db.get_one(req.params.id, table);
//         res.json(user);
//     }
//     catch (err) {
//         console.log(err);
//         res.sendStatus(500);
//     };
// });



router.post("/", async (req, res) => {
    try {
        const salt = await bcrypt.genSalt(10);
        const hashed_pass = await bcrypt.hash(req.body.r_password, salt);
        req.body.r_password = hashed_pass;
        const inserted = await db.post(req.body);
        const inserted_user = await db.get_one(inserted.insertId, table);
        res.json(inserted_user);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/orders", async (req, res) => {
    console.log(req.query)
    try {
        const user = await db.get_users_orders(req.query);
        res.json(user);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/reservations", async (req, res) => {
    console.log(req.query)
    try {
        const restaurant = await db.get_reservations(req.query);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.post("/reservations_msg", async (req, res) => {
    try {
        const restaurant = await db.update_reservation_msg(req.body);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

const aa = {
    "user_name": "",
    "password": "",
    "email": "",
    "phone": "",
    "city": "",
    "country": "",
    "zip": "",
    "address": "",
    "sex": "",
    "language": "",
}

router.post("/registration", async (req, res) => {
    try {
        const salt = await bcrypt.genSalt(10);
        const hashed_pass = await bcrypt.hash(req.body.password, salt);
        req.body.password = hashed_pass;
        const login_inserted = await db.user_registration(req.body);
        // req.body.id = login_inserted.insertId;
        // const profile_inserted = await db.registration(req.body, table_profile);
        // const inserted_restaurant = await db.get_one(inserted.insertId, table);
        res.json(login_inserted);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

const table_login = "user_profile";

router.post("/login", async (req, res) => {
    try {
        console.log(req.body)
        const user = await db.check_user(req.body.user_name, table_login);
        console.log(user);
        if (!user.length) return res.status(401).json({ message: "UserName of Password incorrect" });

        const password_check = await bcrypt.compare(req.body.password, user[0].password);
        if (!password_check) return res.status(401).json({ message: "UserName of Password incorrect" });

        // const rest = await db.get_one(user[0].id, table_profile);

        const access_token = create_jwt(user[0].id, req.body.user_name, "");

        const ref_token = create_jwt(user[0].id, req.body.user_name, process.env.REFRESH_TOCKEN_SECRET);
        // const ref_token = jwt.sign(user[0].id, process.env.REFRESH_TOCKEN_SECRET);

        user[0].password = null;

        res.json({ access_token, ref_token, user: user[0] })
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/session",  async (req, res) => {
    try {
        const token = req.header('a_la_rt');

        const verify = jwt.verify(token, process.env.REFRESH_TOCKEN_SECRET);
        console.log(verify);

        const access_token = create_jwt(verify.id, verify.user_name, "");

        const user = await db.check_user(verify.user_name, table_login);
        // console.log(req.body)
        // const user = await db.check_user(req.body.user_name, table_login);
        // console.log(user);
        // if (!user.length) return res.status(401).json({ message: "UserName of Password incorrect" });

        // const password_check = await bcrypt.compare(req.body.password, user[0].password);
        // if (!password_check) return res.status(401).json({ message: "UserName of Password incorrect" });

        // // const rest = await db.get_one(user[0].id, table_profile);

        // const access_token = create_jwt(user[0].id, req.body.user_name, "");

        // const ref_token = create_jwt(user[0].id, req.body.user_name, process.env.REFRESH_TOCKEN_SECRET);
        // // const ref_token = jwt.sign(user[0].id, process.env.REFRESH_TOCKEN_SECRET);

        // user[0].password = null;

        res.json({ access_token, user: user[0] })
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/provjera", verify, async (req, res) => {
    try {
        console.log("provjera", req.verify_user_name, req.a_token_new)
        res.json({name: req.verify_user_name })
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/token", async (req, res) => {
    try {
        const token = req.header('a_la_rt');
        if(!token) return res.status(403).json({ message: "Unvalid token"});
        
        jwt.verify(token, process.env.REFRESH_TOCKEN_SECRET, (err, verify) => {
            if(err) return res.status(403).json({ message: "Unvalid token"});

            // provjeri iz baze token da li je isti
            
            const access_token_new = create_jwt(verify.id, verify.user_name, "");
            res.json({ new_token: access_token_new })
        });
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});






const create_jwt = (user_id, user_name, ref_token) => {
    if (!ref_token) {
        return jwt.sign({ id: user_id, user_name }, process.env.AUTH_TOCKEN_SECRET, {
            expiresIn: '10s'
        });
    }
    else {
        return jwt.sign({ id: user_id, user_name }, ref_token);
    };
};

module.exports = router;
const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
    const a_token = req.header('a_la_at');
    if (a_token) {
        jwt.verify(a_token, process.env.AUTH_TOCKEN_SECRET, (err, verify) => {
            if (err) return res.status(401).json({ message: "Unvalid token" });

            req.verify_user_name = verify.user_name;
            req.verify_id = verify.id;
            next();
        });
    }
    else {
        return res.status(401).json({ message: "Unvalid token" });
    };
};